\documentclass[10pt]{report}
\usepackage{amsmath, amsthm, amssymb, tikz, mathtools, hyperref, enumerate, paracol}
\usepackage{geometry}
\newcommand{\scinot}[2]{#1\times 10^{#2}}
\newcommand{\bra}[1]{\left<#1\right|}
\newcommand{\ket}[1]{\left|#1\right>}
\newcommand{\dotp}[2]{\left<#1\left.\right|#2\right>}
\newcommand{\rd}[2]{\frac{d#1}{d#2}}
\newcommand{\pd}[2]{\frac{\partial #1}{\partial#2}}
\newcommand{\norm}[1]{\left|\left|#1\right|\right|}
\newcommand{\abs}[1]{\left|#1\right|}
\newcommand{\expvalue}[1]{\left<#1\right>}
\newcommand{\rtd}[2]{\frac{d^2#1}{d#2^2}}
\newcommand{\pvec}[1]{\vec{#1}^{\,\prime}}
\let\Re\undefined
\let\Im\undefined
\DeclareMathOperator{\Re}{Re}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Im}{Im}
\newcommand{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
\usepackage[labelfont=bf, font=scriptsize]{caption}
\everymath{\displaystyle}

\begin{document}

\title{CS4 --- Functional Programming\\ Vanier}
\author{Yubo Su}
\date{}

\maketitle
\tableofcontents

\chapter{05/01/15 --- Introduction}

This class will be taught in Scheme (Racket dialect), Typed Racket (type checking) and Ocaml. Website is on Moodle. Course is not practical but conceptual programming course. Three types of programming are function, imperative, and object-oriented. Scheme is very similar to Javascript, and was initially intended to be Javascript until parentheses became a rightfully-deserved turn off. Haskell is the archetypal functional language, CS115; both Ocaml and Racket support functional programming.

\section{Programming fundamentals}

Let's first examine some fundamentals of programming
\begin{enumerate}[1)]
    \item Infix notation --- the binary operator is in the middle, such as \texttt{3+4}; we use parentheses to specify order. 
    \item Assignment --- Assigning a variable a value, such as \texttt{y=4} or \texttt{x=y+z}.
    \item Function --- Takes an argument and returns a value.
    \item Prefix notation --- Just like functions, the operator proceeds the input, like \texttt{f(x,y)}. An alternative prefix notation separates with commas, like \texttt{(f 3 2)}.
\end{enumerate}

Scheme uses prefix notation such as \texttt{(+ 3 4)} or \texttt{(+ 3 (* 4 5))}. In fact you can write \texttt{(+ 2 3 4)}. Assignment goes \texttt{(define x 3)}.

Lambda means ``this combination represents a function.'' Often we can write things like \texttt{(define a (lambda (r) (* pi (expt r 2))))}.

Comments are proceeded with a semicolon and go until the end of the line. Next time we will talk about the substitution model of evaluation.

\chapter{07/01/15 --- The Substitution Model, booleans and conditionals}

We examine today how expressions are evaluated: the \emph{substitution model}. Recall \texttt{(operator operand1 operand2 \dots)} is the Scheme way. Thus, the three steps to evaluating such an expression are
\begin{itemize}
    \item Evaluate operator
    \item Evaluate operand(s)
    \item Apply operator procedure to operands
\end{itemize}

Notably, we specify that reading the \texttt{+} operator must call the \emph{primitive procedure} corresponding to addition. Note also that \emph{evaluating} the operands takes care of all the nested parentheses that we see all over the place.

There is some special forms which evaluates differently from the above. One is the \texttt{define} protocol, which goes like \texttt{(define x 3)}. We evaluate only the second operand and associate it to the first operand. The \texttt{define} protocol returns the value assigned.

Another is the \texttt{lambda} operator, which returns a procedure; \emph{none} of the operands are evaluated. A procedure can then be called, and the way we apply a procedure to its arguments is to simply evaluate its arguments and then substitute them for the variables in the function \emph{body}. 

\begin{center}
    Note the following \textbf{Syntactic Sugar}:

    \texttt{(define f (lambda (x) (+ x 1)))} is equivalent to \texttt{(define (f x) (+ x 1))}
\end{center}

This is the end of the \emph{substitution model}, which is mechanical enough to be described in terms for a computer.

\section{Booleans, Conditionals}

We first introduce the \emph{boolean}, which contains either a true or false flag (\texttt{\#t} and \texttt{\#f}) respectively. Some functions evaluate to booleans, such as \texttt{(= 2 3)}. Moreover, other functions expect boolean operands such as \texttt{(and \#t \#f)}. 

\begin{center}
    Note that the \texttt{and}, \texttt{or} operators are smart but don't follow the standard evaluation protocol; they \emph{will} short-circuit. They can also take more than two operands.
\end{center}

A typical convention is for functions returning booleans to be named ending with a question mark, called \emph{predicates}. 

The \texttt{if} conditional is the most powerful way to use booleans, comprising the form
\begin{center}
    \texttt{(if <boolean-expression> <true-case> <false-case>)}
\end{center}

Note that now that we have more than one data type, we must touch on types. Scheme does not type check before running code, so type errors show up at runtime.
\chapter{09/01/15 --- Recursion and Iteration}

Let's open with a small math problem; how do we turn a repeating decimal into a rational number? Alternatively, how do we sum an infinite series? Mathematically, this is very easy; the trick in both is to turn the problem into a recursive definition.

\section{Recursion}

Let's try to apply this sort of reasoning to a very simple problem, summing the first $N$ integers. Note that the sum of the first $N$ integers is simply $N + $ the sum of the first $N-1$ integers. Taking care to write in our base case, the full code to do this in Scheme is
\begin{verbatim}
(define (sum-integers n)
    (if (= n 0)
        0
        (+ n (sum-integers (- n 1)))))
\end{verbatim}

In general, recursion requires a three step design pattern
\begin{enumerate}[1.]
    \item Test whether base case
    \item Base case
    \item Recursive case
\end{enumerate}

Proving that a recursive algorithm is correct usually invokes mathematical induction.

To discuss the efficiency of this recursive algorithm, we note that evaluating for $N$ results in a chain of $N$ deferred operations and $N$ total function calls. This is called a \emph{linear recursive} process. A linear recursive process requires an amount of time and space both of which are linear in the size of the input. 

\section{Iteratiion}

Consider another algorithm, summing as we go:
\begin{verbatim}
(define (sum-int n)
    sum-iter 0 n 0)
(define (sum-iter current max sum)
    (if (> current max)
    sum
    (sum-iter (+ 1 current) max (+ current sum))))
\end{verbatim}

Of course we still require $N$ total function calls, but how about the number of deferred operations? Constant! Compare
\begin{paracol}{2}
    \begin{verbatim}
    (sum-integers 3)
    (+ 3 (sum-integers 2))
    (+ 3 (+ 2 (sum-integers 1)))
    ...
    (+ 3 (+ 2 (+ 1 0)))
    \end{verbatim}
    \switchcolumn
    \begin{verbatim}
    (sum-int 3)
    (sum-iter 0 3 0)
    (sum-iter 1 3 0)
    (sum-iter 2 3 1)
    ...
    (sum-iter 4 3 6)
    \end{verbatim}
\end{paracol}

The second algorithm is called a \emph{linear iterative process}. It still makes a linear number of calls but the state of computation is kept in a constant number of state variables, so it requires constant storage space. While both are recursive procedures, the algorithms differ in that recursive processes require pending operations while iterative ones don't. 

Usually the iterative version of an algorithm is preferred, though the recursive is almost always easier to implement and prove correctness. Also, sometimes space isn't the constraining factor and so it proves okay to use the recursive implementation and be lazy. 

Note above that we required a helper function \texttt{sum-iter} to keep track of the state of the program; we will see later how to put helper functions ``inside'' functions that use them.

\section{Fibonacci Numbers}

Consider the age-old Fibonacci numbers. We note that a recursive implementation requires two recursive calls per execution. This is called \emph{tree recursive} instead of linear because calls fan out in a tree. The number of calls is exponential in the size of the argument (the precise form is nontrivial since the tree is not symmetric). The inefficiency of the naive recursive implementation lies in the fact that numbers previously computed are recomputed; we will discuss more about optimizing this out and general asymptotic efficiencies next lecture!

\chapter{12/01/15 --- Algorithms}

\section{Iterative Fibonacci}

Let's finish the Fibbonacci problem we discussed earlier. Consider the code
\begin{verbatim}
(define (fib-iter fnext f cnt)
    (if (= cnt 0)
        f
        (fib-iter (+ fnext f)
            fnext
            (- cnt 1))))
\end{verbatim}

Again, instead of an exponential growth we observe a linear growth. Recall that this is called a linear iterative process, so it requires $n+1$ calls and constant space to solve the problem. 

\textbf{Blurb:} Note that we can use what is called \emph{memoization} to improve the highly inefficient tree-recursive Fibonacci procedure we saw last lecture by memorizing values so we don't have to recalculate. We won't discuss this now but maybe later.

\section{Efficiency of algorithms}

The most important quantifier of the efficiency of an algorithm is the runtime's dependence. We introduce \emph{Big O} notation, such that a function $g(n)$ is $O(f(n))$ if for some $n_0$, there exists $n > n_0, g(n) \leq C f(n)$ for some constant $C$. In other words, $g(n)$ grows no faster than $f(n)$ for sufficiently large $n$.

Since big $O$ is a bound from above, we also need a bound from below, which is satisfied by $\Theta$, big theta notation. Usually we say ``big oh'' for $\Theta$, and we notate it like $O$ already, but technically they are different! $\Theta$ is the lower bound, $O$ is the upper. For example, the recursive Fibonacci algorithm is $O(2^n)$ but $\Theta(\varphi^n), \varphi = 1.618\dots$ the golden ratio.

We care about this because, like in the case of the Fibonacci implementations, the recursive goes like $O(2^n)$ while the iterative goes merely like $O(n)$!.

Note also that all logarithmic behaviors with different bases are equivalent up to a constant and so we don't care about the base in big O notation. 

\section{Internal Procedures}

Suppose we want to write a helper function, say to evaluate $(a + b) * c$, but don't want it to pollute the global namespace. Thus, we can write an internal procedure
\begin{verbatim}
(define (timesPlus a b c)
    (define (plusPlus a b)
        (+ a b))
    (* (plusPlus a b) c))
\end{verbatim}

The advantage is that we have easier code to understand and to write variable names for, but it is generally harder to debug.

\section{\texttt{cond} Evaluation}

The functional way to do else ifs is the \texttt{cond} operator. It uses syntax
\begin{verbatim}
(cond (<test1> <expr1>)
    (<test2> <expr2>)
    (<test3> <expr3>)
    (...)
    (else <expr>)
\end{verbatim}

\chapter{14/01/15 --- Higher order functions}

In math, we have seen functions as arguments. For example, when we write $\sum\limits_{i=0}^{n}f(n)$ the $\Sigma$ is actually a function taking $f(n)$ as an argument! We call this $\Sigma$ a \emph{higher-order function}, and in Scheme we are allowed to define such functions.

Consider this exact problem. We will write the implementation
\begin{verbatim}
(define (sum f low high)
    (if (> low high)
        0
        (+ (f low)
            (sum f (+ low 1) high))))
\end{verbatim}
which is clearly a linear recursive process. Note that if we want, say, a sum of squares, we can call with 
\begin{center}
    \texttt{(sum (lambda (x) (* x x)) 2 4)}
\end{center}

We can also write up the iterative version, for which we will need extra state variables.

Consider the slight extension of the above \texttt{sum} code, which gives the ability to specify the next number in the summation
\begin{verbatim}
(define (gsum f low fnext high)
    (if (> low high)
        0
        (+ (f low)
            (gsum f (fnext low) fnext high))))
\end{verbatim}

Now we can specify a whole class of summations simply by passing \texttt{lambda} functions into \texttt{f} and \texttt{fnext}. Lambda is all powerful!

We can next study how to solve problems, such as sum of prime factors. The general approach will be to successively find the smallest prime factor, sum it, divide it, and repeat.

\section{Lambda functions}

One might wonder why we call these functions ``lambda functions.'' This is because back in 1930s, Alonzo Church saw lambda calculus, the theory of computation of the time, which was based on pure functions, and sought to eliminate as many kinds of data as possible. He kept only one type of data, \emph{functions}. There are then only three types of expressions: variables, functions of one argument, and function applications. Since lambda expressions also give higher-order procedures, it's possible to solve everything with just lambda expressions! That's all.

\chapter{16/01/15 --- COOKIES}

Cookies were brought to class. 

\section{\texttt{Let} construct}

Anyways, we can define something with very local scope with the \texttt{let} construct, via
\begin{verbatim}
(let ([x 10]
    [y (- 2 4)])
    (* x y))
\end{verbatim}

\texttt{x, y} will not have any more scope outside of the \texttt{let} construct. Neato!

We note that this sort of substitution model is eerily reminiscent of functions; \texttt{let} is actually just another way of writing \texttt{lambda}! The following two pieces of code are equivalent
\begin{verbatim}
(let ( (<var1> <expr1>)
    (<var2> <expr2>))
    <body>)

( (lambda (<var1> <var2) <body)
    <expr1> <expr2>)
\end{verbatim}

We also exhibit the syntax \texttt{let*}, which turns into a bunch of nested \texttt{let}s. This is helpful if the later \texttt{let}s are dependent on the earlier ones.

\section{\texttt{begin} clause}

Sometimes, if you want multiple lines under the same execution block (curly braces in C, Java etc.) one can do so by enclosing in \texttt{begin}
\begin{verbatim}
(if (> x 10)
    (begin
        (display x)
        (newline)
        (* x 2))
    (/ x 2))
\end{verbatim}

\texttt{begin} returns the result of the last line only. Note also we used \texttt{display}, \texttt{newline} to print things and to print a new line.

\section{Returning functions}

Now let's get onto the really interesting stuff. Can we return a function? Consider a general function to generate functions that add $n$
\begin{verbatim}
(define (make-addn n)
    (lambda (x) (+ x n)))
\end{verbatim}

Then if we want a function that adds $2$, for example, we can just \texttt{(define add2 (make-addn 2))}.

There becomes a bit of a subtlety here; you cannot substitute into a lambda function's own arguments. So for example, \texttt{(define (weird n) (lambda (n) (+ n n)))}, the \texttt{n} on the inner \texttt{lambda} is \emph{not} substituted! \texttt{lambda} protects its own arguments.

We can obviously substitute multiple layers of lambdas as well; consider the function
\begin{verbatim}
(define multi-add
    (lambda (a)
        (lambda (b)
            (lambda (c)
                (+ a b c)))))
\end{verbatim}
which will return functions for the first few returns until the final one. Such functions are called \emph{curried} functions after logician Haskell Curry.

\chapter{23/01/15 --- Compound Data Types}

We introduce compound data as a way to simplify life. We will motivate composite data and data abstraction, including the \texttt{cons}, \texttt{car}, \texttt{cdr} functions. 

Consider for example complex multiplication. A function that multiplies two complex numbers would have to take \emph{four} arguments, and then you would still need two functions to get the real and imaginary parts. We want to be able to combine data structures.

\section{\texttt{cons}}

The simplest way to construct a data structure. Constructs a composite data item containing the original two, e.g. \texttt{(cons 3 4)}, called a \emph{cons cell} or \emph{cons pair}. Displayed as \texttt{'(3 . 4)}. 

We retrieve components of a cons pair with two operations, \texttt{car} and \texttt{cdr}, retrieving the first and second items respectively. These stand for \emph{C}ontents of \emph{A}ddress \emph{R}egister and \emph{C}ontents of \emph{D}ecrement \emph{R}egister respectively. So for example
\begin{verbatim}
(define a (cons 3 4))
(car a)
-> 3
(cdr a)
-> 4
\end{verbatim}

\texttt{cons} works on any data type and the trio of commands are not special forms.

We can obviously implement our complex numbers like this. It is recommended however to not use \texttt{cons}, \texttt{car}, \texttt{cdr} directly though, for the sake of abstraction layers. This is especially beautiful when writing something like, say, a quadratic equation, where we will never have to see \texttt{cons}, \texttt{car}, \texttt{cdr} ever again! This is helpful because we can change lower level implementations without changing the higher-level code. For example we can implement complex numbers in the form of Scheme vectors instead. This \emph{abstraction barrier} is key to good code, as we only have to work within one abstraction layer at once.
\chapter{25-28/01/15 --- List Processing}

We introduce some type predicates
\begin{itemize}
    \item \texttt{pair?} operator which says whether something is a \texttt{cons} pair. We use it like \texttt{(pair? (cons 3 4))}
    \item \texttt{number?} for integers, rationals, reals
    \item \texttt{boolean?} for booleans
    \item \texttt{procedure?} if something is a procedure
\end{itemize}

Found the pattern yet?

We also introduce the \texttt{(error "string" <data>)} construct to break execution and return to the interpreter. 

\section{Lists}

We want to make a list of data, since we know how to use pairs now. The end-of-list character is just the null character, which can be tested for by \texttt{null?} and is generated by \texttt{(list)} the empty list. Intead of using a bunch of linked \texttt{cons} pairs we can just directly use \texttt{(list 1 2 3)}. Use \texttt{cons} to concatenate to a \texttt{list}. As expected, this doesn't change the original list; \texttt{list2 = (cons <item> list1)} will not change \texttt{list1}.

Oftentimes we want to store a data point, which will just be a \texttt{list} of \texttt{cons} pairs. If we want to perform an operation on this list, say, find the max, we will need to process the list recursively, \texttt{cdr}-ing our way through. This can either be a recursive or iterative implementation, depending on our guidelines. 

Another common problem is to find the \emph{location} of the maximum. This is easy to do by just finding the maximum and then iterating through item-by-item; can you do it in a single pass using the tools we have so far?

\section{Designing list algorithms}

There are a few distinct steps in designing any algorithm:
\begin{itemize}
    \item What are the inputs and outputs?
    \item What are the \emph{base cases} and how do we handle them?
    \item What do I do with the \emph{first few elements} and the \emph{rest of} the list?
\end{itemize}

Recall that the base case is defined as something that can be solved without having to make any recursive calls. 

Let's get some practice: consider if we want to remove an element of a list with some value \texttt{x}. First, what are base cases? Empty list, return list. Any other base cases? Initial element contains \texttt{x}, return \texttt{cdr} of list. Otherwise, recursively call with \texttt{cdr} of list. The full code thus looks like
\begin{verbatim}
(define (remove-by-x a-list x)
  (cond ((null? a-list) a-list)
        ((= (get-x (car a-list)) x)
          (cdr a-list))
        (else
          (cons (car a-list)
                (remove-by-x (cdr a-list) x)))))
\end{verbatim}

We can also consider inserting an element into an ordered, say descending, list. Let's run through our algorithm again. First, we identify inputs/outputs as an input list with an element to be inserted and an output list. The base cases are if the list is empty (return list containing our single element) and if \texttt{x} is is greater than the first element of the list (directly insert into front). If neither of these base cases, then what do we do with the rest of the list? We make the first element the first element of a new ordered list and we insert the result into the rest of the list recursively.

What is the time complexity of this insertion? Clearly it is $O(N)$ as worst case scenario it parses through the entire list. It is thus a linear recursive procedure. (Note: while there is clearly a binary search algorithm that optimizes this, I guess it's not Vanier's duty to point this out).

\section{Sorting!}

The big topic. We may want to take an unsorted list of results and sort them. The simplest sort would just be to have an unsorted list as input and insert into a sorted list as we iterate through the input list; this is called \emph{insertion sort}. Apparently there is a super-elegant recursive implementation but Vanier talks only about the iterative one which any baffoon can do and I will not cover.

The complexity of insertion sort is $O(N^2)$ beacuse each element of the unsorted list has to walk through the entire sorted list to be placed correctly. Can we do better? We can split the list in halves and sort the halves, then merge together! This is \emph{merge sort}. A careful analysis shows that the time complexity of merge sort is $O(N\log N)$. Assuming we can implement \texttt{even-half}, \texttt{odd-half} and some \texttt{merge-in-order} that takes the even-indexed, odd-indexed elements of a list and merges two ordered lists respectively. Then our program looks like
\begin{verbatim}
(define (merge-sort a-list)
  (if (or (null? a-list) (null? (cdr a-list)))
      a-list
      (merge-in-order (merge-sort (even-half a-list)) 
          (merge-sort (even-half a-list)))))
\end{verbatim}

It is very easy to write \texttt{odd-half} by just recursively \texttt{cdr}-ing twice at a time. It is even easier then to write \texttt{even-half} by just odd-half-ing the \texttt{cdr}! We exhibit \texttt{merge-in-order} for illustrative purposes
\begin{verbatim}
(define (merge-in-order alist blist) 
   (cond ((null? alist) blist) 
         ((null? blist) alist) 
         ((goes-before? (car alist) (car blist)) 
          (cons (car alist) 
            (merge-in-order (cdr alist) blist))) 
         (else  
          (cons (car blist) 
            (merge-in-order alist (cdr blist))))))
\end{verbatim}

Note that merge sort at $O(N\log N)$ is much more efficient than insertion sort $O(N^2)$. The two recursive algorithms have very different recursion patterns: insertion sort recurses over the natural structure of the data \texttt{car}, \texttt{cdr}, while merge sort \emph{generatively} recurses, i.e. recurses on subsets of data generated by the user. The formal is simpler and more natural but the latter more general and more powerful!

\chapter{02/02/15 --- Quoting, tagged data, and type dispatching}

Today we discuss the special form \texttt{quote}, which is like a string literal from other language: we need a way to distinguish a symbol from a variable. The contents of the \texttt{quote} special form are called a ``symbol.'' Syntactic sugar also means we can use a \emph{single} quotation mark equivalently to a \texttt{quote} expression, e.g. \texttt{'a} is the same as \texttt{(quote a)}. Note that quoting a list simply quotes everything inside it, and so \texttt{'(1 2 3)} is easier to read than \texttt{(list 1 2 3)}. 

Note a shortcut with \texttt{car}s and \texttt{cdr}s, we can do things like \texttt{caddr}, up to $3$ \texttt{a}s and \texttt{d}s. 

\section{Tagged data}

Suppose that before, when we had a list of \texttt{cons} pairs, we now have a list of lists, with another entry, namely a \texttt{tag} of some sort (perhaps the units?) Units are super important and has cost NASA billions of dollars, so let's learn how to do things the right way. 

If we are to define units, we also need implementations of operations such as addition that are unit-aware, such that meters and feet are added separately or a conversion factor is inserted. Alternatively, we can implement these operations and forcibly cast everything to a single unit, e.g. feet. This is called \emph{coercion} and is probably illegal in many countries. We can then overload addition to add multiple types of units using a \texttt{cond} statement etc. 

If we want to do this overload though, it's expensive to add more code on the overloaded function! We instead will want to add these conversion codes into the data types that are coming in; this will lead us to the idea of OOP, or object oriented programming.

\chapter{04/02/15 --- Message-passing}

We now want to be able to build operators with data (this should sound suspiciously like object oriented programming)! The problem with tagged data is that the generic operators had to know about all the types of tags, which is a pain particularly when adding data types. Instead, we want data that knows how to perform certain operations on \emph{itself}, so we make the data ``intelligent.'' This is what we call an \emph{object}. 

\section{Variable numbers of arguments}

This is super handy. What if we want an unspecified number of arguments to a function? We simply use a \texttt{.}! Consider
\begin{verbatim}
(define (our-add . args)
  (apply + args))
\end{verbatim}
which will add everything passed. This is because \texttt{.} stores all arguments into \texttt{args}; note that \texttt{apply} allows us to apply \texttt{+} to the entire list. 

Note that this is really just another \texttt{lambda} sugar. \texttt{(define (our-add . args))} actually desugars into
\begin{verbatim}
(define our-add
  (lambda args
  ...))
\end{verbatim}

Of course we can use \texttt{(foo x y . args)} to get some fixed and some variable arguments.

\section{Message-passing}

The idea behind message-passing is to ``communicate with data'' by requesting data or a computation. The \emph{only} way to interact with data is by sending messages to it under this formalism; the internals are kept \emph{private}. Now let's try the implementation; this will look very complex at first because they create and return \texttt{lambda}s, but the implementations are often of similar structure.
\begin{verbatim}
(define (make-meter x)
  (lambda (op . args)
    (cond
      ((eq? op 'get-length) x)
      ((eq? op 'get-type) 'meter)
      ((eq? op 'add)
        (make-meter
          (+ x ((car args) 'get-length))))
      (else (error "unknown op" op)))))
\end{verbatim}

Note thus that \texttt{make-meter} returns a procedure \texttt{lambda} with the first argument the operation \texttt{op} and the rest \texttt{args} arguments to the operation. The key is that the data item itself knows how to perform the relevant operations.

Our above implementation isn't exactly complete; we need type checking in \texttt{'add} as well! But then this becomes really complicated, because type-checking is going to have to be reimplemented with each new data type. We instead probably want to implement a \texttt{to-base-units} function to do this instead. 

Note that now we have a little stand-off: with message-passing implementations, it's really easy to add new types, but adding new operations is complicated because every type needs its own operation! With tagged data, it's pretty easy to add new operations but hard to keep types verified! This is called the \emph{expression problem}, and no universal solutions are accepted. What middle ground can we walk? One such solution is similar to what we have explored already: \emph{generic functions} that act differently on different data types.

In general though message-passing is extremely powerful for grouping data and keeping helper functions confined and not floating around in some global namespace.

\section{Quasiquoting}

Small little construct, works similarly to \texttt{quote} but can escape it using \texttt{unquote} forms, such as \texttt{(quasiquote (a b c (unquote d) e))}. Also has a syntactic sugar associated, using \emph{backquote} \texttt{`} instead of \texttt{'}. Similarly, can use \texttt{,} to unquote, such as \texttt{`((a b c ,(d) e))}. Also, can use \texttt{unquote-splicing}, such that if string inside construct is a list, we don't just copy the list over but copy elements out; can try \texttt{`(a b c ,\@lst e)}. 

\chapter{09/02/15 --- Interfaces}

Interfaces are a standard set of operations that we can apply to a \emph{class of types}. Think about all the various radio players that have their own implementations of pause, play etc. In our message-passing system then, the data system itself is in charge of implementing the interface, and an object can implement any number of interfaces.

The power of interfaces arises when we can define functions that \emph{only} require the interface functions, because then these functions work over any implementation of the interface and reduces maintenance difficulties. Interfaces are very natural in object-oriented programming languages.

\section{Sets as interfaces}

Consider a mathematical set and the functionality it must implement: add, remove, contains?, union, intersection, empty? and more. We can just start with these and declare an interface for these functions. Suppose we implement the message-passing interface naively for all of these functions. We can then reason about the operational performance of these implementations
\begin{itemize}
    \item \texttt{add} --- Probably constant, just \texttt{cons}
    \item \texttt{remove} --- Probably linear, need to search through whole while remove all copies
    \item \texttt{contains?} --- Probably linear, need to search all
\end{itemize}

However, we can equally well implement it smarter, perhaps by ordering it. We could implement as a tree instead, which would improve some performances but would worsen others (such as \texttt{add}). We won't discuss the specific implementations, but note that both implementations work in lieu of the other, because they implement the same interface!

\section{Mutation, Imperative programming}

Up until now, all \texttt{define} have been constant in time; we now introduce the ability to change a variable's value! Note that now the substitution model won't be correct anymore, because values can change during runtime. We have only exmained Scheme as a functional language, but that doesn't mean it's limited to so much\dots

The syntax for this is \texttt{set! x (+ 1 x)}, pronounced set-bang! Note that \texttt{set!} doesn't return anything, just mutates variable in place.

Recall the keyword \texttt{begin}, which worked like \texttt{(begin f1 f2 f3 ... fn)}, where each \texttt{f} is executed in kind and the return is the return of \texttt{fn}. This was useless to us before, since the preceeding function calls couldn't affect anything in \texttt{fn}, but now with mutations this becomes suddenly very important. 

It is a convention to add \texttt{!} after every function that mutates something.

Note that the substitution model breaks down with mutators; we no longer use just values with mutators but \emph{variables}, which the substitution model cannot handle. We must introduce the \emph{environment model}.

\chapter{11/02/15 --- The Environment Model}

I'm too lazy to draw the diagrams in this, so this might be a really short set of notes.

We must augment the substitution model. Recall that we discussed \texttt{define} as an \emph{association}, a mapping between a variable and a value. This is now at the forefront of the environment model. We then discuss key components
\begin{itemize}
    \item A \emph{binding} is an association between a name and a Scheme \emph{value}. 
    \item A \emph{frame} is a collection of bindings stored in computer memory. Values are looked up in frames.
    \item Frames have an \emph{enclosing environment} which is another frame. So if a lookup within a frame fails, we go to parent frame and continue lookup. 
    \item An environment is a linked chain of frames ending in the \emph{global environment}.
    \item Environment diagrams are trees made out of connected frames, such that when we are within any single frame we look ``up'' and see a linked list of environments, though there may be environments we cannot see.
\end{itemize}

The Scheme interpreter starts in the global environment. Evaluating code then can create new frames and new environments. All code being evaluated must be done within the context of an environment (to make for lookup); this is called the current environment. We then exhibit a few rules for this nonsense about how we transition between environments
\begin{itemize}
    \item \texttt{define} --- creates a new binding in the current environment.
    \item \texttt{set!} --- changes an old binding in the current environment, but not necesarily current frame; never creates a new binding!
    \item \texttt{lambda} --- pair comprising the text of the lambda expression and a pointer to the environment \emph{in which it was created} (called the \emph{enclosing environment}). Text is divided into parameters and body. 
    \item Applying a procedure to operands --- Construct a new frame with parent as the environment of the procedure, bind parameters to arguments in new frame, evaluate in context of new frame.
    \item After procedure --- Kill frame.
\end{itemize}

Note that primitives such as \texttt{+} don't get their own environment b/c they're primitive. 

Let's do an example, \texttt{(define (f x y) (+ (* 3 x) (* -4 y) 2))}. This desugars into a \texttt{lambda}, so we create the \texttt{lambda} pair pointing to the current environment, and then we bind \texttt{f} to the \texttt{lambda} pair in the current environment. 

More examples in \texttt{lecture13.pdf}. 
\end{document}
